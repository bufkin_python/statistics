#! /usr/bin/env/python311

import math
from statistics import stdev, variance, pstdev

if __name__ == '__main__':
    numbers = [0, 1, 2, 3, 10000, 20000, 30000]

    print(f"Original input {numbers}")
    print(f"sample variance: {stdev(numbers)}")
    print(f"variance: {variance(numbers)}")
    print(f"population variance: {pstdev(numbers)}")
